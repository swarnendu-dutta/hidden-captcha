<?php

/**
 * @file
 * Contains \Drupal\hidden_captcha\Form\HiddenCaptchaSettingsForm.
 */

namespace Drupal\hidden_captcha\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class HiddenCaptchaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hidden_captcha_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hidden_captcha.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hidden_captcha.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('hidden_captcha.settings');

    $form = [];
    $form['hidden_captcha_label'] = [
      '#type' => 'textfield',
      '#title' => t('Hidden text field label'),
      '#description' => t("This is the hidden captcha form field's label, describing the expected input.<br />" . "<strong>It is highly recommended to change it!</strong><br />" . "The label should be as \"machine-readable\" as possible, encouraging spambots to fill in the field. An example might be a simple math challenge.<br />" . "The label will only be visible to people who do not have CSS enabled and to robots."),
      '#default_value' => $config->get('hidden_captcha_label', 'Website URL'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
